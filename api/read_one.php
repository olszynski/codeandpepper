<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../config.php';
include_once '../models/People.php';

// Instantiate DB & connect
$db = new db();

// Instantiate object
$people = new People($db);

// Get ID
$people->id = isset($_GET['id']) ? $_GET['id'] : die();

// Get record
$result = $people->read_one();

$num = $result->rowCount();

// Check if any record
if ($num > 0) {
  $row = $result->fetch(PDO::FETCH_ASSOC);
  $people->name = $row['name'];
  $people->mass = $row['mass'];

  $data_arr = array(
    'id' => $people->id,
    'name' => $people->name,
    'mass' => $people->mass
  );
  http_response_code(200);
  // Make JSON
  print_r(json_encode($data_arr));
} else {
  http_response_code(404);
  // No Record
  echo json_encode(
    array('message' => 'No Record Found')
  );
}
