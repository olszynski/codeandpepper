<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: DELETE');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../config.php';
include_once '../models/People.php';

// Instantiate DB & connect
$db = new db();

$people = new People($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

if (!isset($data->id)) {
  echo json_encode(
    array('message' => 'Record Not Deleted')
  );
  die();
}
$people->validation('ok', 1, $data->id, 'Deleted');
// Set ID to update
$people->id = $data->id;

// Delete record
if ($people->delete()) {
  echo json_encode(
    array('message' => 'Record Deleted')
  );
} else {
  echo json_encode(
    array('message' => 'Record Not Deleted')
  );
}
