<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../config.php';
include_once '../models/People.php';

// Instantiate DB & connect
$db = new db();

// Instantiate object
$people = new People($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

if (!isset($data->name) or !isset($data->mass)) {
  echo json_encode(
    array('message' => 'Record Not Created')
  );
  die();
}

$people->validation($data->name, $data->mass, 0, 'Created');
$people->name = $data->name;
$people->mass = $data->mass;

// Create record
if ($people->create()) {
  http_response_code(200);
  echo json_encode(
    array('message' => 'Record Created')
  );
} else {
  http_response_code(404);
  echo json_encode(
    array('message' => 'Record Not Created')
  );
}
