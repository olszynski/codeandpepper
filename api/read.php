<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../config.php';
include_once '../models/People.php';

// Instantiate DB & connect
$db = new db();

// Instantiate object
$people = new People($db);

$result = $people->read();

// Get row count
$num = $result->rowCount();

// Check if any records
if ($num > 0) {
  // people array
  $people_arr = array();

  while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    $person = array(
      'id' => $id,
      'name' => $name,
      'mass' => $mass
    );

    // Push to "data"
    array_push($people_arr, $person);
   
  }
  http_response_code(200);
  // Turn to JSON & output
  print_r(json_encode($people_arr));
} else {
  http_response_code(404);
  // No Record
  echo json_encode(
    array('message' => 'No Record Found')
  );
}
