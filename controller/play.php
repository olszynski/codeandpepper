<?php
include_once '../config.php';
include_once '../models/People.php';

// Instantiate DB & connect
$db = new db();

// Instantiate object
$people = new People($db);

$permanent_enemy = isset($_GET['permanent_enemy']) ? $_GET['permanent_enemy'] : 0;

$all = isset($_GET['all']) ? $_GET['all'] : 0;


if ($all == 1) {
  // Get all records
  $result = $people->allPeople();
} else {
  // Get random records
  $result = $people->play($permanent_enemy);
}

$num = $result->rowCount();

// Check if any records
if ($num > 1) {
  // people array
  $people_arr = array();

  while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    $person = array(
      'id' => $id,
      'name' => $name,
      'mass' => $mass
    );
    // Push to "data"
    array_push($people_arr, $person);
  }
  // Turn to JSON & output
  print_r(json_encode($people_arr));
} else {

  // No Record
  echo json_encode(
    array('message' => 'No Record Found')
  );
}
