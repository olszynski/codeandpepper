$(document).ready(function () {
	var permanent_enemy = 0;

	//fill field select with people to play with permanent enemy
	jQuery.ajax(
		{
			url: '../codeandpepper/play',
			dataType: "json",
			data: { all: 1 },
			success: function (data) {
				var json = JSON.parse(JSON.stringify(data));
				if (json.length > 0) {

					var select = $("<select></select>").attr("id", "permanent_enemy").attr("name", "permanent_enemy");
					select.append($("<option></option>").attr("value", '0').text(''));
					$.each(json, function (index, json) {
						select.append($("<option></option>").attr("value", json.id).text(json.name));
					});
					$("#enemy").html(select);
				}
			},
			error: function (error) {
				console.log("Error:");
				console.log(error);
			}
		});
    //start play
	$("#play").click(function () {
		jQuery.ajax(
			{
				url: '../codeandpepper/play',
				dataType: "json",
				data: { permanent_enemy: permanent_enemy },
				success: function (data) {
					var json = JSON.parse(JSON.stringify(data));
					if (json.length > 0) {
						var win;
						var index0;
						var index1;
						if (json[0].id == permanent_enemy) {
							index0 = 1;
							index1 = 0;
						} else {
							index0 = 0;
							index1 = 1;
						}
						$("#name1").html(json[index0].name);
						$("#name2").html(json[index1].name);
						$("#mass1").html(json[index0].mass);
						$("#mass2").html(json[index1].mass);
						$(".name, .mass").show();
						$("#winner").remove();
						if (json[index0].mass > json[index1].mass) {
							win = 1;
							$('#score1').html(parseInt($('#score1').html()) + 1);
						} else if (json[index0].mass < json[index1].mass) {
							win = 2;
							$('#score2').html(parseInt($('#score2').html()) + 1);
						} else {
							win = 0;
						}
						if (win > 0) {
							$('<div id="winner">Winner</div>').insertAfter("#mass" + win);
						} else {
							$('<div id="winner">Draw</div>').insertBefore("#play");
						}
					} else {
						alert('No people to play');
					}
				},
				error: function (error) {
					console.log("Error:");
					console.log(error);
				}
			});
	})
	//select permanent enemy
	$("#setEnemy").click(function () {
		permanent_enemy = $('#permanent_enemy').val();
		reset();
		if (permanent_enemy > 0) {
			jQuery.ajax(
				{
					url: '../codeandpepper/api/read/' + permanent_enemy,
					dataType: "json",
					method: "GET",
					success: function (data) {
						var json = JSON.parse(JSON.stringify(data));
						$("#name2").html(json.name);
						$("#mass2").html(json.mass);
						$(".name, .mass").show();
						$("#name1, #mass1").html('');
						$("#winner").remove();
					},
					error: function (error) {
						console.log("Error:");
						console.log(error);
					}
				});
		}
	})
	//reset scores
	$("#reset").click(function () {
		reset();
	})
})
function reset(){
	$('#score1, #score2').html("0");
}

