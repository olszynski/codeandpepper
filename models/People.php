<?php
class People
{
  private $conn;
  private $table = 'people';

  public $id;
  public $name;
  public $mass;

  public function __construct($db)
  {
    $this->conn = $db;
  }
  public function read()
  {
    $per_page = 100;
    $offset = 0;

    $per_page = isset($_GET['per_page'] ) && is_numeric($_GET['per_page']) ? ($_GET['per_page']) : $per_page;
    $offset = isset($_GET['page']) && is_numeric($_GET['page']) ? ($_GET['page'] * $per_page) : $offset;

    // Create query
    $query = 'SELECT * from ' . $this->table . ' order by id asc limit :per_page  offset :offset';

    // Prepare statement
    $data = $this->conn->prepare($query);

    // Bind pagination
    $data->bindParam(':per_page', $per_page);
    $data->bindParam(':offset', $offset);

    // Execute query
    $data->execute();

    return $data;
  }

  public function read_one()
  {
    // Create query
    $query = 'SELECT * from ' . $this->table . '  WHERE id = ?';

    // Prepare statement
    $data = $this->conn->prepare($query);

    // Bind ID
    $data->bindParam(1, $this->id);

    // Execute query
    $data->execute();

    return $data;
  }
  public function create()
  {
    // Create query
    $query = 'INSERT INTO ' . $this->table . '(name, mass) values ( :name,:mass)';

    // Prepare statement
    $data = $this->conn->prepare($query);

    // Bind data
    $data->bindParam(':name', $this->name);
    $data->bindParam(':mass', $this->mass);

    try {
      // Execute query
      if ($data->execute()) {
        return true;
      }
    } catch (PDOException $e) {
    }

    return false;
  }

  // Update Record
  public function update()
  {
    // Create query
    $query = 'UPDATE ' . $this->table . '
                                SET name = :name, mass = :mass
                                WHERE id = :id';

    // Prepare statement
    $data = $this->conn->prepare($query);

    // Bind data
    $data->bindParam(':name', $this->name);
    $data->bindParam(':mass', $this->mass);
    $data->bindParam(':id', $this->id);

    try {
      // Execute query
      if ($data->execute()) {
        return true;
      }
    } catch (PDOException $e) {
    }
    return false;
  }

  // Delete Record
  public function delete()
  {
    // Create query
    $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

    // Prepare statement
    $data = $this->conn->prepare($query);

    // Bind data
    $data->bindParam(':id', $this->id);

    // Execute query
    if ($data->execute()) {
      return true;
    }

    return false;
  }

  public function play($permanent_enemy)
  {
    // Create query  
    if ($permanent_enemy == 0) {
      $query = ' SELECT * FROM ' . $this->table . ' order by random() limit 2';
    } else {
      $query = ' (SELECT * FROM public.people  where id<>:permanent_enemy order by random() limit 1)
               union 
               SELECT * FROM public.people  where id=:permanent_enemy ';
    }

    // Prepare statement
    $data = $this->conn->prepare($query);

    // Bind data
    if ($permanent_enemy > 0) {
      $data->bindParam(':permanent_enemy', $permanent_enemy);
    }

    // Execute query
    $data->execute();

    return $data;
  }
  public function allPeople()
  {
    // Create query 
    $query = ' SELECT * FROM ' . $this->table . ' order by name asc';

    // Prepare statement
    $data = $this->conn->prepare($query);

    // Execute query
    $data->execute();

    return $data;
  }

  public function validation($name, $mass, $id, $operation)
  {

    $error = 0;

    if (!is_numeric($mass) or !is_numeric($id)) {
      $error = 1;
    }
    if (strlen($name) == 0 or strlen($name) > 100) {
      $error = 1;
    }

    if ($error == 1) {
      http_response_code(404);
      echo json_encode(
        array('message' => 'Record Not ' . $operation)
      );
      die();
    }
  }
}
